# ansible-blackhole

Playbooks to manage my Raspberry Pis.

Run `ansible-playbook -i hosts playbook.yml`

## Todos
- Only restart mopidy if required
- Update mopidy.conf from playbook

## Ideas
- Use Icecast or Snapcast to stream to more devices in the house - https://oyvn.no/multi-room-audio-with-snapcast
